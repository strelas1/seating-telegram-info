import telebot
import requests
import psycopg2
from config import DB_USER, DB_NAME, DB_PASSWORD, IP_HOST, SITE_HOST, TOKEN


bot = telebot.TeleBot(TOKEN)
conn = psycopg2.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASSWORD, host=IP_HOST)
cursor = conn.cursor()

def create_table():
    cursor.execute("CREATE TABLE IF NOT EXISTS players (player_id INTEGER, chat_id BIGINT UNIQUE)")
    conn.commit()


@bot.message_handler(commands=['start'])
def main(message):
    bot.send_message(message.chat.id,
                     f'Привет! Введи свой ник ФСМ (с учетом регистра и цифр в нике).\n'
                     f'Либо обычную версию ника в формате /pair ник (пример: /pair Strelas).\n'
                     f'Если обычная версия ника не ищется, проверьте как он записан на mafbase.ru или уточните у организатора\n'
                     )


@bot.message_handler()
def nickname(message):
    nick = message.text
    orig_nick = ''
    response = requests.get(f'{SITE_HOST}/api/availablePlayers')

    if message.entities and message.entities[0].type == 'bot_command' and message.text.startswith('/pair'):
        orig_nick = message.text[6:].strip()

    if response.status_code == 200:
        flag = False
        union_info = response.json()
        for player in union_info['players']:
            if orig_nick != '':
                if player.get('nickname') == orig_nick:
                    flag = True
            else:
                if player.get('fsmNickname') == nick:
                    flag = True
            if flag == True:
                chat_id = message.chat.id
                nick_id = player['id']
                cursor.execute(
                    "INSERT INTO players (player_id, chat_id) VALUES (%s, %s) ON CONFLICT (chat_id) DO UPDATE SET player_id = EXCLUDED.player_id",
                    (nick_id,chat_id))
                conn.commit()
                bot.send_message(message.chat.id, 'Авторизация прошла успешно. Теперь вы будете получать индивидуальные уведомления о турнире.')
                break
        if flag == False:
            bot.send_message(message.chat.id, 'Игрок с данным ником не найден')
    else:
        bot.send_message(message.chat.id, 'Неизвестная ошибка')






create_table()
print(f'start bot with {TOKEN}')
bot.polling(none_stop=True)
cursor.close()
conn.close()

