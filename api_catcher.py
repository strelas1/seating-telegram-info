from flask import Flask, request, jsonify
import requests
import psycopg2
from config import TOKEN, DB_NAME, DB_USER, DB_PASSWORD, IP_HOST

app = Flask(__name__)



def search_id(player_id):
    conn = psycopg2.connect(dbname=DB_NAME, user=DB_USER, password=DB_PASSWORD, host=IP_HOST)
    cursor = conn.cursor()
    cursor.execute("SELECT chat_id FROM players WHERE player_id = %s", (player_id,))
    result = cursor.fetchall()
    chat_id_list = []
    for i in result:
        if i[0] not in chat_id_list:
            chat_id_list.append(i[0])
    cursor.close()
    conn.close()
    print('following chat ids for %s: %s', (player_id, chat_id_list))
    return chat_id_list

def send_telegram_message(chat_id, message):
    url = f"https://api.telegram.org/bot{TOKEN}/sendMessage"
    data = {"chat_id": chat_id, "text": message}
    response = requests.post(url, json=data)
    return response.json()

@app.route('/send-player-info', methods=['POST'])
def send_player_info():
    data = request.json
    print('recieved send-player-info with %s', (data))
    game = data["game"]
    table = data["table"]
    place = data["place"]
    player_id = data["playerId"]

    chat_id = search_id(player_id)

    if len(chat_id) != 0:
        for id in chat_id:
            message = f"Проходим по столам! Начинается {game} игра, ты играешь за столом {table} на номере {place}"
            send_telegram_message(id, message)
        return jsonify({"success": True})
    else:
        return jsonify({"success": False, "error": "Ошибка, игрок не найден"})


@app.route('/send-text', methods=['POST'])
def send_text():
    data = request.json
    player_id = data["playerId"]
    text = data["text"]
    print('recieved send-text with %s', (data))

    chat_id = search_id(player_id)

    if len(chat_id) != 0:
        for id in chat_id:
            send_telegram_message(id, text)
        return jsonify({"success": True})
    else:
        return jsonify({"success": False, "error": "Ошибка, игрок не найден"})



@app.route('/send-referee-info', methods=['POST'])
def send_referee_info():
    data = request.json
    player_id = data["playerId"]
    game = data["game"]
    players = data["players"]
    print('recieved send-referee-info with %s', (data))

    chat_id = search_id(player_id)

    if len(chat_id) != 0:
        for id in chat_id:
            message = f"Состав на {game} игру:\n"
            for num, player in enumerate(players, start=1):
                message += f"{num}. {player}\n"
            send_telegram_message(id, message)
        return jsonify({"success": True})
    else:
        return jsonify({"success": False, "error": "Ошибка, судья не найден"})

