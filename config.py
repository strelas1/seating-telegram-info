import os
from dotenv import load_dotenv

load_dotenv()
TOKEN = os.getenv('TOKEN')
DB_NAME = os.getenv('DB_NAME')
DB_USER = os.getenv('DB_USER')
DB_PASSWORD = os.getenv('DB_PASSWORD')
SITE_HOST = os.getenv('SITE_HOST')
IP_HOST = os.getenv('IP_HOST')